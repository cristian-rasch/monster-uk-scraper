# Monster::UK::Scraper

monster.co.uk job scraper

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'monster-uk-scraper', git: 'git@bitbucket.org:cristian-rasch/monster-uk-scraper.git', require: 'monster/uk/scraper'
```

And then execute:

    $ bundle

## Usage

```ruby
require "monster/uk/scraper"
require "pp"

# Options include:
#   - annual_salary - minimum annual salary. See #annual_salary_options
#   - hourly_rate - minimum hourly rate. See #hourly_rate_options
#   - salary_info - only return jobs that have salary information (false by default)
#   - categories - up to 2 job categories to search for. See #category_options
#   - industries - up to 2 industries to search in. See #industry_options
#   - date_rage - how far back to search for results
#                 (defaults to 14 days, valid values are 0 - a.k.a today, 1, a.k.a yesterday,
#                  3, 7, 14, 30 and nil a.k.a no date range)
#   - job_types - restrict the search to the specified job types
#                 (defaults to temporary and contract jobs)
#   - career_level - desired career level. See #career_level_options
#   - education_level - desired education level. See #education_level_options
#   - years_of_experience - desired years of relevant experience. See #years_of_experience_options
#   - limit - how many jobs to retrieve (defaults to 20, max. 20)
#   - proxies - a list of http proxies to tunnel web traffic through, e.g. %w(http://1.2.3.4:8888/ http://5.6.7.8:8888/) -
#               defaults to reading them from the MONSTER_PROXIES env var or nil if missing
#   - logger - a ::Logger instance to log error messages to
monster_uk = Monster::UK::Scraper.new

# Job search
jobs = monster_uk.job_search("web developer", date_range: 7, limit: 1,
                             categories: "IT/Software Development",
                             industries: ["Computer Software", "Computer/IT Services"])
pp jobs.first
# #<struct Monster::US::Scraper::Job
#  title="Senior Java / Web Developer - Contractor",
#  url=
#   "http://jobview.monster.co.uk:80/Senior-Java-Web-Developer-Contractor-Job-Portsmouth-South-East-Southern-146356615.aspx?jobPosition=2",
#  id="146356615",
#  employer="Informatiq Consulting",
#  created_on=2015-03-01 17:19:09 UTC,
#  location="Portsmouth, Southern PO110DG",
#  salary="350.00 - 410.00 £ /per day",
#  category=nil,
#  occupations=nil,
#  industry="Internet Services",
#  education_requirements="HND/HNC or equivalent",
#  experience_requirements=nil,
#  career_level="Experienced (Non-Manager)",
#  description_html=
#   "<img src=\"http://counter.adcourier.com/Z2FyeWIuODA0MzcuNDc5QGluZm9ybWF0aXEuYXBsaXRyYWsuY29t.gif\"><br><strong>Snr Software Developer /Architect - Defence Sector Projects>
```

