require File.expand_path("test/test_helper")
require File.expand_path("lib/monster/uk/scraper")

module Monster
  module UK
    class ScraperTest < Minitest::Test
      def setup
        @monster_uk = Scraper.new
        @keywords = "web developer"
      end

      def test_education_level_filtering
        jobs = @monster_uk.job_search(@keywords, limit: 1, education_level: "Bachelor's Degree")
        assert_equal 1, jobs.size
      end

      def test_career_level_filtering
        jobs = @monster_uk.job_search(@keywords, limit: 1, career_level: "Experienced (Non-Manager)")
        assert_equal 1, jobs.size
      end

      def test_industry_filtering
        jobs = @monster_uk.job_search(@keywords, limit: 1, industries: ["Computer Software", "Computer/IT Services"])
        assert_equal 1, jobs.size
      end

      def test_category_filtering
        jobs = @monster_uk.job_search(@keywords, limit: 1, categories: ["IT/Software Development", "R&D/Science"])
        assert_equal 1, jobs.size
      end

      def test_hourly_rate_filtering
        jobs = @monster_uk.job_search(@keywords, limit: 1, hourly_rate: 25, salary_info: true)
        assert_equal 1, jobs.size
      end

      def test_annual_salary_filtering
        jobs = @monster_uk.job_search(@keywords, limit: 1, annual_salary: 30000, salary_info: true)
        assert_equal 1, jobs.size
      end

      def test_date_range_filtering
        jobs = @monster_uk.job_search(@keywords, limit: 1, date_range: 3)
        assert_equal 1, jobs.size
      end

      def test_date_job_type_filtering
        jobs = @monster_uk.job_search(@keywords, limit: 1, job_types: "contract")
        assert_equal 1, jobs.size
      end

      def test_getting_three_jobs
        jobs = @monster_uk.job_search(@keywords, limit: 3)
        assert_equal 3, jobs.size
      end

      def test_max_page_option
        jobs = @monster_uk.job_search(@keywords, max_page: 1, limit: 100)
        assert_operator jobs.size, :<=, 50
      end

      def test_passing_in_a_callback_to_job_search
        job = nil
        @monster_uk.job_search(@keywords, limit: 1) do |j|
          job = j
        end
        refute_nil job
      end
    end
  end
end
