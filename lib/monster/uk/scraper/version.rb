module Monster
  module US
    class Scraper; end
  end

  module UK
    class Scraper < US::Scraper
      VERSION = "0.0.1"
    end
  end
end
