require "monster/uk/scraper/version"
require "monster/us/scraper"

module Monster
  module UK
    class Scraper < US::Scraper
      # Options include:
      #   - annual_salary - minimum annual salary. See #annual_salary_options
      #   - hourly_rate - minimum hourly rate. See #hourly_rate_options
      #   - salary_info - only return jobs that have salary information (false by default)
      #   - categories - up to 2 job categories to search for. See #category_options
      #   - industries - up to 2 industries to search in. See #industry_options
      #   - date_rage - how far back to search for results
      #                 (defaults to 14 days, valid values are 0 - a.k.a today, 1, a.k.a yesterday,
      #                  3, 7, 14, 30 and nil a.k.a no date range)
      #   - job_types - restrict the search to the specified job types
      #                 (defaults to temporary and contract jobs)
      #   - career_level - desired career level. See #career_level_options
      #   - education_level - desired education level. See #education_level_options
      #   - years_of_experience - desired years of relevant experience. See #years_of_experience_options
      #   - limit - how many jobs to retrieve (defaults to 20, max. 20)
      #   - proxies - a list of http proxies to tunnel web traffic through, e.g. %w(http://1.2.3.4:8888/ http://5.6.7.8:8888/) -
      #               defaults to reading them from the MONSTER_PROXIES env var or nil if missing
      #   - logger - a ::Logger instance to log error messages to
      def initialize(options = {})
        super
      end

      # Options include:
      #   - annual_salary - minimum annual salary. See #annual_salary_options
      #   - hourly_rate - minimum hourly rate. See #hourly_rate_options
      #   - salary_info - only return jobs that have salary information (false by default)
      #   - categories - up to 2 job categories to search for. See #category_options
      #   - industries - up to 2 industries to search in. See #industry_options
      #   - date_rage - how far back to search for results
      #                 (defaults to 14 days, valid values are 0 - a.k.a today, 1, a.k.a yesterday,
      #                  3, 7, 14, 30 and nil a.k.a no date range)
      #   - job_types - restrict the search to the specified job types
      #                 (defaults to temporary and contract jobs)
      #   - career_level - desired career level. See #career_level_options
      #   - education_level - desired education level. See #education_level_options
      #   - years_of_experience - desired years of relevant experience. See #years_of_experience_options
      #   - limit - how many jobs to retrieve (defaults to 20, max. 20)
      def job_search(keywords, options = {})
        super
      end

      def annual_salary_options
        @annual_salary_options ||= salary_nodes.
                                    css('[data-id="1"] option').
                                    map { |option| option["value"] }[1..-1].
                                    map(&:to_i)
      end

      def hourly_rate_options
        @hourly_rate_options ||= salary_nodes.
                                   css('[data-id="2"] option').
                                   map { |option| option["value"] }[1..-1].
                                   map(&:to_i)
      end

      def category_options
        @category_options ||= search_page.
                                at_css('select[title="Category"]').
                                css("option").
                                map(&:text)[1..-1]
      end

      def industry_options
        @industry_options ||= search_page.
                                at_css('select[title="Industry"]').
                                css("option").
                                map(&:text)[1..-1]
      end

      def career_level_options
        @career_level_options ||= search_page.
                                    css('select[title="Career Level"] option').
                                    map(&:text)[1..-1]
      end

      def education_level_options
        @education_level_options ||= search_page.
                                      css('select[title="Education Level"] option').
                                      map(&:text)[1..-1]
      end

      def years_of_experience_options
        @years_of_experience_options ||= search_page.
                                          css('select[title="Years of Relevant Experience"] option').
                                          map{ |option| option["value"] }[2..-1].
                                          map(&:to_i)
      end

      private

      def customize_search_url(search_url, options)
        search_url.tap do |jobs_url|
          filters = []

          categories = Array(options[:categories]) & category_options
          unless categories.empty?
            filters.concat(categories.first(2).map { |category| category.gsub(/[^a-z]/i, "-") })
          end

          industries = Array(options[:industries]) & industry_options
          unless industries.empty?
            filters.concat(industries.first(2).map { |industry| industry.gsub(/[^a-z]/i, "-") })
          end

          jtypes = Array(options[:job_types] || job_types)
          jtypes.map!(&:capitalize)
          jtypes &= DEFAULT_JOB_TYPES
          jtypes = DEFAULT_JOB_TYPES if jtypes.empty?
          filters.concat(jtypes)

          jobs_url << CGI.escape(filters.join(" "))
          jobs_url << "_"
          jobs_url << param_checksum(categories: categories, industries: industries, job_types: jtypes)

          annual_salary = (Array(options[:annual_salary]) & annual_salary_options).first
          if annual_salary
            jobs_url << (jobs_url.include?("?") ? "&" : "?")
            jobs_url << "salmin=#{annual_salary}&saltyp=1"
          end

          hourly_rate = (Array(options[:hourly_rate]) & hourly_rate_options).first
          if hourly_rate
            jobs_url << (jobs_url.include?("?") ? "&" : "?")
            jobs_url << "salmin=#{hourly_rate}&saltyp=2"
          end

          if options[:salary_info]
            jobs_url << (jobs_url.include?("?") ? "&" : "?")
            jobs_url << "nosal=false"
          end

          career_level = (Array(options[:career_level]) & career_level_options).first
          if career_level
            career_level.gsub!("/", " ")
            career_level.gsub!(/[(,)]/, "")
            career_level.gsub!(" ", "-")
            jobs_url << (jobs_url.include?("?") ? "&" : "?")
            jobs_url << "lv=#{career_level}"
          end

          education_level = (Array(options[:education_level]) & education_level_options).first
          if education_level
            education_level.gsub!("/", " ")
            education_level.gsub!(/[(,)]/, "")
            education_level.gsub!(" ", "-")
            jobs_url << (jobs_url.include?("?") ? "&" : "?")
            jobs_url << "eid=#{CGI.escape(education_level)}"
          end

          years_of_experience = (Array(options[:years_of_experience]) & years_of_experience_options).first
          if years_of_experience
            jobs_url << (jobs_url.include?("?") ? "&" : "?")
            jobs_url << "ye=#{years_of_experience}-years"
          end
        end
      end

      def param_checksum(options)
        checksum = ""
        checksum << ("4" * options[:categories].size)
        checksum << ("3" * options[:industries].size)
        checksum << ("8" * options[:job_types].size)
      end

      def salary_nodes
        @salary_nodes ||= search_page.css('select[key="salmin"]')
      end

      def search_page
        @search_page ||= begin
                           advance_search_url = "#{base_url}/AdvancedSearch.aspx"
                           Nokogiri::HTML(open_through_proxy(advance_search_url))
                         end
      end

      def base_url
        @base_url ||= "http://jobsearch.monster.co.uk".freeze
      end
    end
  end
end

